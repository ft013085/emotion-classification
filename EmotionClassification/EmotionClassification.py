import cv2
from deepface import DeepFace

def detect_and_label_faces(frame, face_cascade):
    # Convert the frame to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Perform face detection
    faces = face_cascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

    # Loop through detected faces
    for (x, y, w, h) in faces:
        # Extract the face region
        face_roi = gray[y:y+h, x:x+w]

        # Perform emotion recognition on the face region
        result = DeepFace.analyze(frame[y:y+h, x:x+w], actions=['emotion'], enforce_detection=False)

        # Check if emotion analysis was successful
        if 'emotion' in result:
            # Get the dominant emotion
            dominant_emotion = result['emotion']

            # Draw bounding box around the face and label with emotion
            cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
            cv2.putText(frame, f'Emotion: {dominant_emotion}', (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 255, 0), 2)

    return frame

def main():
    # Load pre-trained face detection model
    face_cascade = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')

    # Prompt the user to choose between live camera analysis or analyzing a video file
    choice = input("Enter 'L' for live camera analysis or enter the name of the video file to analyze (e.g., 'reference.mp4'): ")

    if choice.lower() == 'l':
        # Capture video from webcam
        cap = cv2.VideoCapture(0)
    else:
        # Capture video from file
        video_file = input("Enter the name of the video file: ")
        cap = cv2.VideoCapture(video_file)

    # Main loop
    while True:
        # Capture frame-by-frame
        ret, frame = cap.read()
        if not ret:
            break

        # Detect and label faces with emotion
        result_frame = detect_and_label_faces(frame, face_cascade)

        # Display the resulting frame
        cv2.imshow('Facial Expression Recognition Result', result_frame)

        # Break the loop when 'q' is pressed
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    # Release capture and close windows
    cap.release()
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()

